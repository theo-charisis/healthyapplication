package com.example.healthyapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class QuizHelper2 extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "space1";
    // tasks table name
    private static final String TABLE_QUEST = "quest";
    // tasks Table Columns names
    private static final String KEY_ID = "qid";
    private static final String KEY_QUES = "question";
    private static final String KEY_ANSWER = "answer"; // correct option
    private static final String KEY_OPTA = "opta"; // option a
    private static final String KEY_OPTB = "optb"; // option b
    private static final String KEY_OPTC = "optc"; // option c
    private static final String KEY_OPTD = "optd"; // option d

    private SQLiteDatabase dbase2;

    public QuizHelper2(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db2) {
        dbase2 = db2;
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_QUEST + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_QUES
                + " TEXT, " + KEY_ANSWER + " TEXT, " + KEY_OPTA + " TEXT, "
                + KEY_OPTB + " TEXT, " + KEY_OPTC + " TEXT, " + KEY_OPTD + " TEXT)";
        db2.execSQL(sql);
        addQuestion();
        // db2.close();
    }

    private void addQuestion() {
        Question q1 = new Question("Dame Nellie Melba, a famous Australian opera singer, had a fruity dessert named after her; which fruit is used in the making of it?", "Strawberry", "Peach", "Pear","Plum", "Peach");
        this.addQuestion(q1);
        Question q2 = new Question("There is a fruit juice that can increase the potency of some medication, even causing an overdose. Which fruit juice is this?", "Apple", "Orange", "Grapefruit", "Pineapple","Grapefruit");
        this.addQuestion(q2);
        Question q3 = new Question("What is a nectarine? Is it:", "A cross between a peach and a plum", "A cross between an an apple and a peach", "A type of peach", "A type of orange","A type of peach");
        this.addQuestion(q3);
        Question q4 = new Question("On which continent did broccoli originate?", "Asia", "South America", "Africa","Europe", "Europe");
        this.addQuestion(q4);
        Question q5 = new Question("Every year there are, on average, how many banana-related accidents?", "20", "300", "100","50", "300");
        this.addQuestion(q5);
        Question q6 = new Question("The edible berry of a woody vine, how is a kiwifruit otherwise known? ", "Japanese Plum", "Chinese Gooseberry", " Indian Fig","Asian Pear", "Chinese Gooseberry");
        this.addQuestion(q6);
        Question q7 = new Question("In some countries, this fruit may be called a plantain?", "Dates", "Damson ", "Banana","Persimmon", "Banana");
        this.addQuestion(q7);
        Question q8 = new Question("Papain is a natural digestive aid that is found naturally in:", "Mango", "Papaya", "Banana","Lychee", "Papaya");
        this.addQuestion(q8);
        Question q9 = new Question("There is a museum in Belgium dedicated to:", "Chocolate", "Detective Poirot", "Strawberries","Beer", "Strawberries");
        this.addQuestion(q9);
        Question q10 = new Question("What fruit is often referred to as the \"World's Smelliest Fruit\"? ", "Lemon", "Durian", "Banana","Ugli Fruit", "Durian");
        this.addQuestion(q10);
        Question q11 = new Question("Because it has a high quercitin content this fruit may help relieve hayfever.", "Raspberry", "Strawberry", "Melon","Grape", "Raspberry");
        this.addQuestion(q11);
        Question q12 = new Question("A loganberry is a cross between a blackberry and a:", "Blueberry", "Raspberry", "Blackcurrant","Strawberry", "Raspberry");
        this.addQuestion(q12);
        Question q13 = new Question("According to American research, which fruit is No. 1 when it comes to antioxidants?", "Blueberry", "Orange", "Pear","Cherry", "Blueberry");
        this.addQuestion(q13);
        Question q14 = new Question("In January 2009, what was named as the state fruit of Ohio?", "Blueberry", "Cranberry", "Paw paw","Apple", "Cranberry");
        this.addQuestion(q14);
        Question q15 = new Question("The fruit of the physalis plant is also known as:", "Kiwi", "Fig", "Olive","Cape gooseberry", "Cape gooseberry");
        this.addQuestion(q15);
        Question q16 = new Question("Kumquat is Chinese for:", "Gold orange", "Green orange", "Tangerine","Satsuma", "Gold orange");
        this.addQuestion(q16);
        Question q17 = new Question("In Greek mythology, in the stories of Persephone, what is said to be the fruit of the underworld?", "Grape", "Pineapple", "Pomegranate","Orange", "Pomegranate");
        this.addQuestion(q17);
        Question q18 = new Question("Which of these fruits is not native to North America?", "Blueberry", "Apple", "Cranberry","Grape", "Apple");
        this.addQuestion(q18);
        Question q19 = new Question("The stones from which fruit were once used in bed-warming pans?", "Plum", "Avocado", "Cherry","Peach", "Cherry");
        this.addQuestion(q19);
        Question q20 = new Question("What percentage of the watermelon is water?", "34%", "80%", "66%","92%", "92%");
        this.addQuestion(q20);
        Question q21 = new Question("Which of these fruits grows on a tree?", "Pomegranate", "Banana", "Passionfruit", "Feijoa","Pomegranate");
        this.addQuestion(q21);
        Question q22 = new Question("Which fruit has the highest oil content?", "Peach", "Avocado", "Olive","Mango", "Olive");
        this.addQuestion(q22);
        // END
    }

    @Override
    public void onUpgrade(SQLiteDatabase db2, int oldV, int newV) {
        // Drop older table if existed
        db2.execSQL("DROP TABLE IF EXISTS " + TABLE_QUEST);
        // Create tables again
        onCreate(db2);
    }

    // Adding new question
    public void addQuestion(Question quest) {
        // SQLiteDatabase db2 = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_QUES, quest.getQUESTION());
        values.put(KEY_ANSWER, quest.getANSWER());
        values.put(KEY_OPTA, quest.getOPTA());
        values.put(KEY_OPTB, quest.getOPTB());
        values.put(KEY_OPTC, quest.getOPTC());
        values.put(KEY_OPTD, quest.getOPTD());

        // Inserting Row
        dbase2.insert(TABLE_QUEST, null, values);
    }

    public List<Question> getAllQuestions() {
        List<Question> quesList2 = new ArrayList<Question>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_QUEST;
        dbase2 = this.getReadableDatabase();
        Cursor cursor = dbase2.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Question quest = new Question();
                quest.setID(cursor.getInt(0));
                quest.setQUESTION(cursor.getString(1));
                quest.setANSWER(cursor.getString(2));
                quest.setOPTA(cursor.getString(3));
                quest.setOPTB(cursor.getString(4));
                quest.setOPTC(cursor.getString(5));
                quest.setOPTD(cursor.getString(6));

                quesList2.add(quest);
            } while (cursor.moveToNext());
        }
        // return quest list
        return quesList2;
    }

}
