package com.example.healthyapplication;

/**
 * Created by ΤΕΟ on 14/7/2017.
 */

import android.provider.BaseColumns;

public class TaskContact {
    public static final String DB_NAME = "theo1";
    public static final int DB_VERSION = 1;

    public class TaskEntry implements BaseColumns {
        public static final String TABLE = "tasks";

        public static final String COL_TASK_TITLE = "title";
    }
}
