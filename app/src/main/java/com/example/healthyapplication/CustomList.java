package com.example.healthyapplication;
/**
 ** Created by ΤΕΟ on 19/5/2017.
 */
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] name;
    private final String[] details;
    private final Integer[] imageId;
    public CustomList(Activity context,
                      String[] name,String[] details, Integer[] imageId) {
        super(context, R.layout.listview, name);
        this.context = context;
        this.name = name;
        this.details = details;
        this.imageId = imageId;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.listview, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtname);
        TextView txtDetails = (TextView) rowView.findViewById(R.id.txtdetails);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(name[position]);
        txtDetails.setText(details[position]);
        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}